<?php

use farrosoft\wpinstaller\commands\Core;
use farrosoft\wpinstaller\commands\Db;
use farrosoft\wpinstaller\commands\Menu;
use farrosoft\wpinstaller\commands\Option;
use farrosoft\wpinstaller\commands\Page;
use farrosoft\wpinstaller\commands\Theme;
use farrosoft\wpinstaller\commands\Post;

require_once 'scripts/install-helper.php';

class Installer {
	//	private $menu_name;

	public function __construct () {
		//		$this->menu_name = 'Header menu';

		$this->db();
		$this->core();
		$this->themes();
		$this->plugins();
		$this->options();
		$this->menus();
		$this->pages();
		$this->posts();
	}

	private function db () {
		DB::drop();
		DB::create();
	}

	private function core () {
		Core::install(getenv('WP_SITEURL'));
		Core::salts('regenerate');
	}

	private function themes () {
		Theme::activate(getenv('THEME_NAME'));
	}

	private function plugins () {
		//Plugin::activate('wp-codeception');
	}

	private function options () {
		Option::update('blogname', getenv('THEME_NAME'));
//            Option::rewrite('/%postname%/');
		Option::deleteAllContent();
	}

	private function menus () {
		//		Menu::create( $this->menu_name );
		//		Menu::assign( $this->menu_name, "header-menu" );
	}

	private function pages () {
		Page::createPage([ 'title' => 'Front page', 'slug' => 'front-page' ]);
		Page::createPage([ 'title' => 'Authentication', 'slug' => 'auth', 'page_template' => 'auth.php' ]);
		Page::createPage([ 'title' => 'Stripe Api', 'slug' => 'stripe-api', 'page_template' => 'stripe.php' ]);

		//		Page::createPage( [
		//			'title'     => 'Главная',
		//			'slug'      => 'main',
		//			'menu_name' => $this->menu_name
		//		] );

	}

	private function posts () {
            Post::createPost(['title' => 'Hello Man!', 'post_type' => 'post']);
	}
}

$installer = new Installer();